package edu.ntnu.idatt2001.alekol.Cardgame;

import edu.ntnu.idatt2001.alekol.Cardgame.Model.DeckOfCards;
import edu.ntnu.idatt2001.alekol.Cardgame.Model.HandOfCards;
import edu.ntnu.idatt2001.alekol.Cardgame.Model.PlayingCard;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.*;

public class PlayingCardTest {

    final String[] correctDeckName = {"S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "S11", "S12", "S13", "H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9", "H10", "H11", "H12", "H13", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10", "D11", "D12", "D13", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10", "C11", "C12", "C13"};
    PlayingCard[] cards;
    HandOfCards hand;
    DeckOfCards deck = new DeckOfCards();

    public void fillArrayWithCards(int n){
        DeckOfCards fillerDeck = new DeckOfCards();
        cards=new PlayingCard[n];
        for(int i=0; i<n; i++){
            cards[i]=fillerDeck.getDeck()[i];
        }
        hand = new HandOfCards(cards);
    }

    @Nested
    class ConstructorTests {
        @Test
        public void canCreateADeck(){
            DeckOfCards newDeck = new DeckOfCards();

            assertEquals(Arrays.stream(newDeck.getDeck())
                    .distinct().count(), correctDeckName.length);
            assertEquals(correctDeckName[0], newDeck.getDeck()[0].toString());
            assertEquals(correctDeckName[51], newDeck.getDeck()[51].toString());
        }

        @Test
        public void canCreateAHand(){
            fillArrayWithCards(8);
            HandOfCards hand = new HandOfCards(cards);
            assertEquals(cards, hand.getHand());
        }

        @Test
        public void cannotCreateHandOfCardsWithLessThanFiveCards(){
            DeckOfCards fillerDeck = new DeckOfCards();
            cards=new PlayingCard[3];
            for(int i=0; i<3; i++){
                cards[i]=fillerDeck.getDeck()[i];
            }
            assertThrows(IllegalArgumentException.class,()-> new HandOfCards(cards));
        }
    }

    @Nested
    class HandCheckTests{
        @Test
        public void getTheCorrectSumOfAHand(){
            fillArrayWithCards(5);
            assertEquals(1+2+3+4+5,hand.getSumOfHand());
        }

        @Test
        public void getAllCardsByDiamondSuitInAHandOf28Cards(){
            fillArrayWithCards(28);
            final String correctString = "D1 D2";
            assertEquals(correctString, hand.getAllCardsBySuit('D'));
        }

        @Test
        public void checkIfHandHasQueenOfSpades(){
            final PlayingCard s12=new PlayingCard('S',12);
            fillArrayWithCards(12);
            assertTrue(hand.hasSpecifiedCard(s12.getSuit(),s12.getFace()));
        }
        @Test
        public void checkIfHandDoesNotHaveQueenOfSpades(){
            final PlayingCard s12=new PlayingCard('S',12);
            fillArrayWithCards(11);
            assertFalse(hand.hasSpecifiedCard(s12.getSuit(),s12.getFace()));
        }

        @Test
        public void checkForFlushWhenTheHandHasFiveHearts(){
            fillArrayWithCards(5);
            assertTrue(hand.hasFlush());
        }

        @Test
        public void checkIfHandHasStraightWhenItDoes(){
            fillArrayWithCards(5);
            assertTrue(hand.hasStraight());
        }
    }

    @Nested
    class DeckOfCardsTests{


        @Test
        public void removeCardRemovesTheSpecifiedCard(){
            deck.removeCard(0);
            assertEquals("S2", deck.getDeck()[0].toString());
        }
        @Test
        public void dealHandDealsOutCorrectAmountOfCards(){
            assertEquals(5,deck.dealHand(5).getHand().length);
            assertEquals(10,deck.dealHand(10).getHand().length);
        }
        @Test
        public void dealHandThrowsExceptionIfHansSizeIsLessThanFive(){
            assertThrows(IllegalArgumentException.class,()-> deck.dealHand(2));
        }
        @Test
        public void cardsAreRemovedFromDeckWhenHandIsDealt(){
            deck.dealHand(5);
            assertEquals(47, deck.getDeck().length);
        }
    }

}
