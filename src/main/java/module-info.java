/**
 * HEY
 */
module Cardgame {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    exports edu.ntnu.idatt2001.alekol.Cardgame.Controller;
    exports edu.ntnu.idatt2001.alekol.Cardgame.Model;
    exports edu.ntnu.idatt2001.alekol.Cardgame.View;
}
