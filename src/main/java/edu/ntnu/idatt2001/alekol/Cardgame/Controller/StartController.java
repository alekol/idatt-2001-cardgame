package edu.ntnu.idatt2001.alekol.Cardgame.Controller;

import edu.ntnu.idatt2001.alekol.Cardgame.Model.DeckOfCards;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class StartController {

    @FXML
    public Button startButton;

    private Stage stage;
    private Scene scene;


    public void startButton(ActionEvent e) {
        try{
            stage=(Stage) ((Node)e.getSource()).getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("sceneTwo.fxml"));
            scene=new Scene(root);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (Exception exception){
            exception.printStackTrace();
            System.out.println(exception.getMessage());
            System.out.println(exception.getCause());
        }
    }
}
