package edu.ntnu.idatt2001.alekol.Cardgame.Model;

import edu.ntnu.idatt2001.alekol.Cardgame.Model.HandOfCards;
import edu.ntnu.idatt2001.alekol.Cardgame.Model.PlayingCard;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private PlayingCard[] deck;

    public DeckOfCards(){
        this.deck=new PlayingCard[52];
        int i=0;
        for (char c : suit) {

            for (int j = 1; j <= 13; j++) {
                deck[j-1+13*i]=new PlayingCard(c, j);
            }
            i++;
        }
    }

    public PlayingCard[] getDeck() {
        return deck;
    }

    public HandOfCards dealHand(int n){
        PlayingCard[] cards = new PlayingCard[n];
        Random randomIndex = new Random();
        for(int i=0; i<n;i++){
            cards[i]=drawCard(randomIndex.nextInt(deck.length-1));
        }
        HandOfCards hand = new HandOfCards(cards);
        if(Arrays.stream(hand.getHand()).anyMatch(Objects::isNull)){
            throw new NullPointerException("The playing deck does not have enough cards");
        }
        else{
            return hand;
        }
    }

    public PlayingCard drawCard(int n){
        return removeCard(n);
    }

    public PlayingCard removeCard(int n){
        final PlayingCard removedCard = deck[n];
        PlayingCard[] newDeck=new PlayingCard[deck.length-1]; // to make sure the size is 1 less (since a card is being removed)
        for(int i=0;i< newDeck.length-1;i++){
            if(i==n){
                continue;
            } else if (i<n){
                newDeck[i]=deck[i];
            } else{
                newDeck[i-1]=deck[i];
            }
        }
        deck=newDeck;
        return removedCard;
    }

    @Override
    public String toString() {
        return "DeckOfCards{" +
                "deck=" + Arrays.toString(deck) +
                '}';
    }
}
