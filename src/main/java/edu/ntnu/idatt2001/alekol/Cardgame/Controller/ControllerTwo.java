package edu.ntnu.idatt2001.alekol.Cardgame.Controller;

import edu.ntnu.idatt2001.alekol.Cardgame.Model.DeckOfCards;
import edu.ntnu.idatt2001.alekol.Cardgame.Model.HandOfCards;
import edu.ntnu.idatt2001.alekol.Cardgame.Model.PlayingCard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.ResourceBundle;

public class ControllerTwo implements Initializable {
    public DeckOfCards deck = new DeckOfCards();
    public HandOfCards hand;

    @FXML
    public TextField numberOfCards;
    public TextArea showText;
    public Button dealHandButton;
    public ListView<String> infoHeader;
    public ListView<String> handInfo;
    public ImageView cardImage1;
    public ImageView cardImage2;
    public ImageView cardImage3;
    public ImageView cardImage4;
    public ImageView cardImage5;
    public Button showHandButton;
    public ComboBox<String> suitCheckComboBox;

    private String[] info=new String[4];
    private ArrayList<ImageView> showCards =new ArrayList<>();
    private final String[] header={"Sum", "Straight", "Flush", "Cards of given suit"};
    private final String[] suits={"Hearts", "Diamonds", "Spades", "Clubs"};
    private final Image backOfCard = new Image((getClass().getResourceAsStream("/ImageFiles/backOfCard.png")));


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        showCards.add(cardImage1);
        showCards.add(cardImage2);
        showCards.add(cardImage3);
        showCards.add(cardImage4);
        showCards.add(cardImage5);
        suitCheckComboBox.getItems().addAll(suits);
    }

    public void dealHand(ActionEvent event){
        if(readNumberOfCards()<5){
            display("Hand size too small", "A hand must contain 5 or more cards");
        } else{
            try{
                hand = deck.dealHand(readNumberOfCards());
                showText.setText("Your hand: \n" + hand.toString());
                showCards.forEach(s->s.setImage(backOfCard));
            } catch(NullPointerException e){
                deck=new DeckOfCards();
                hand=deck.dealHand(readNumberOfCards());
                showText.setText("Your hand: \n" + hand.toString());
            }
        }
    }

    public void checkHand(ActionEvent event){
        editHeader();
        editInfo();
    }

    private void editHeader(){
        infoHeader.getItems().clear();
        infoHeader.getItems().addAll(header);
    }

    private void editInfo(){
        handInfo.getItems().clear();
        handInfo.edit(0);
        info[0]=String.valueOf(hand.getSumOfHand());
        info[1]=String.valueOf(hand.hasStraight());
        info[2]=String.valueOf(hand.hasFlush());
        info[3]=checkForSuit();
        handInfo.getItems().addAll(info);
    }

    public int readNumberOfCards(){
        return Integer.parseInt(numberOfCards.getText());
    }

    private String getImageFile(PlayingCard card){
        String pngName="";
        String pngFace=String.valueOf(card.getFace());
        String fullSuit="";
        if(card.getSuit() == 'C'){
            fullSuit="clubs";
        } else if(card.getSuit()=='D'){
            fullSuit="diamonds";
        } else if(card.getSuit()=='H'){
            fullSuit="hearts";
        } else if(card.getSuit()=='S'){
            fullSuit="spades";
        }
        if(card.getFace()==1){
            pngFace="ace";
        } else if(card.getFace()==11){
            pngFace="jack";
        } else if(card.getFace()==12){
            pngFace="queen";
        } else if(card.getFace()==13){
            pngFace="king";
        }
        String cardAsPng="/ImageFiles/"+pngFace+"_"+"of_"+fullSuit+".png";
        return cardAsPng;
    }
    private void displayCards(HandOfCards hand){
        Image image = null;
        for(int i=0; i<5; i++){
            image=new Image(Objects.requireNonNull(getClass().getResourceAsStream(getImageFile(hand.getHand()[i]))));
            showCards.get(i).setImage(image);
        }
        if(hand.getHand().length>5){
            display("Too many cards", "Could not display a hand length greater than 5");
        }
    }
    public void showHand(ActionEvent event){
        displayCards(hand);
    }

    private void display(String title, String message) {
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);
        window.setMinHeight(250);

        Label label = new Label();
        label.setText(message);

        Button closeButton = new Button("Ok");
        closeButton.setOnAction(e -> window.close());

        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, closeButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
    }

    private String checkForSuit(){
        String defaultReturn = "No cards";
        String suit = suitCheckComboBox.getValue();
        if(suit==null){
            return defaultReturn;
        }else if(suit.equalsIgnoreCase("Hearts")){
            return hand.getAllCardsBySuit('H');
        } else if(suit.equalsIgnoreCase("Diamonds")){
            return hand.getAllCardsBySuit('D');
        } else if(suit.equalsIgnoreCase("Spades")){
            return hand.getAllCardsBySuit('S');
        } else if (suit.equalsIgnoreCase("Clubs")){
            return hand.getAllCardsBySuit('C');
        }
        return defaultReturn;
    }
}
