package edu.ntnu.idatt2001.alekol.Cardgame.Model;


import edu.ntnu.idatt2001.alekol.Cardgame.Model.PlayingCard;

import java.util.Arrays;
import java.util.OptionalInt;


public class HandOfCards {
    private PlayingCard[] hand;

    public HandOfCards(PlayingCard[] cards) {
        if(!(cards.length >4)) throw new IllegalArgumentException("Hand size must be greater than or equal to 5");
        this.hand = cards;
    }

    public PlayingCard[] getHand() {
        return hand;
    }

    public long getSumOfHand(){
        return Arrays.stream(hand).mapToInt(card-> card.getFace()).sum();
    }


    public String getAllCardsBySuit(char suit){
        StringBuilder sb = new StringBuilder();

        Arrays.stream(hand)
                .filter(card->card.getSuit()==suit)
                .forEach(s-> sb.append(s).append(" "));

        sb.deleteCharAt(sb.length()-1);
        return sb.toString();
    }

    public boolean hasSpecifiedCard(char suit, int face){
        return Arrays.stream(hand)
                .anyMatch(s-> s.getSuit()==suit && s.getFace()==face);
    }

    public boolean hasFlush(){
        return Arrays.stream(hand)
                .map(s-> s.getSuit())
                .distinct().count()==1;
    }
    public boolean hasStraight(){
        OptionalInt lowestValueOptional=Arrays.stream(hand).mapToInt(PlayingCard::getFace).min();
        if(lowestValueOptional.isEmpty()) return false;
        int lowestValue = lowestValueOptional.getAsInt();

        OptionalInt highestValueOptional = Arrays.stream(hand).mapToInt(PlayingCard::getFace).max();
        if(highestValueOptional.isEmpty()) return false;
        int highestValue= highestValueOptional.getAsInt();

        int difference = highestValue-lowestValue;
        if(difference!=hand.length) return false;

        for(int i =lowestValue;i<=highestValue;i++){
            int finalI = i;
            if(Arrays.stream(hand).mapToInt(s->s.getFace()).anyMatch(s-> s==finalI)){
                continue;
            } else return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Arrays.stream(hand).forEach(s->sb.append(s.toString()).append(" "));
        return sb.toString();
    }
}
